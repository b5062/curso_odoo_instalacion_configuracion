Instancia Dockerizada de Odoo 15.0 con Base de datos incluida (Luis Millan <lmillan131@gmail.com>)
    Esta instancia esta pensada para ser utilizada con fines didacticos, tiene una configuración básica funcional con la siguiente distribución.
        - Carpeta config: almacena el archivo de configuración de Odoo.
        - Carpeta extra-addons: esta vacia por defecto pero sirve para almacenar los modulos adicionales, recuerde que cada carpeta que se coloque en ese lugar podra ser considerada un modulo adicional por lo tanto es importante que la carpeta que coloque ahí debe tener un archivo __manifest__.py dentro.
        - Archivo docker-compose.yml: Archivo estructural del Docker-Compose, en este se definen los servicios que seran construidos.
        - Dockerfile: Archivo de construcción del Servicio Web, este archivo tiene la finalidad de contemplar las diferentes configuraciones que debe tener el contenedor del servicio Web (Odoo) para su correcto funcionamiento (Instalación de librerias, permisologia, etc...)
    Instrucciones de Instalación
        Antes de iniciar debe tener en cuenta que todos los comandos a continuación debera ejecutarlos siempre dentro de esta carpeta, es decir, debe abrir un terminal bien sea de windows o linux justo dentro de esta carpeta.
        1.- Descargue y descomprima esta carpeta.
        2.- Asegurese de que las direcciones de carpetas esten acordes con su sistema operativo por ejemplo:
                Dirección de SO Windows = C:\Users\Luis\Documents\curso_odoo_instalacion_configuracion
                Dirección de SO Linux = /home/lmillan/Documents
            Note que los sistemas utilizan diferentes barras inclinadas, este detalles es causante de muchos dolores de cabezas para los que estan iniciando.
            estas direcciones las podra modificar en el archivo docker-compose.yml.
        3.- Ubiquese dentro de la carpeta 15OdooCommunity y abra un terminal.
        4.- En el terminal ejecute el siguiente comando:
            docker-compose up
        5.- Si los pasos se ejecutaron correctamente usted debe tener un sistema levantado en el puerto 8069, podra acceder a el desde el navegador con localhost:8069
    Comandos Utiles de Docker
        - docker-compose up (Iniciar/Construir Contenedores)
        - docker-compose stop (detener Contenedores)
        - docker-compose rm (Borrar contenedores)
        - docker ps -a (Listar contenedores)
        - docker-compose build (Construir contenedores)
    Esta instancia cuenta con dos servicios:
        -Servicio Web Odoo: definido en el archivo docker-compose.yml e inicializado en el archivo Dockerfile
        -Servicio Postgres 11.0: Definido completamente en el archivo docker-compose.yml